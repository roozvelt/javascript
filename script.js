"use strict";
function matrix(){
	var matrixInput = [
		[1, 2, 3, 4],
		[5, 6, 7, 8],
		[9, 10, 11, 12],
		[13, 14, 15, 16]
	];

	var matrixOutput = [
		[0, 0, 0, 0],
		[0, 0, 0, 0],
		[0, 0, 0, 0],
		[0, 0, 0, 0]
	];

	var length = matrixInput.length;
	var i, j;

	for (i = 0; i < length; i++) {
		for (j = 0; j < length; j++) {
			matrixOutput[i][j] = matrixInput[length - 1 - j][i];
		}
	};
	return (matrixOutput);
};

console.log(matrix());
